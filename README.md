# small university Application
This project is a small simple web service application.

## Technologies used
1. Jax-rs
2. Jax-ws
3. Glassfish7
4. AerospikeCe
5. Docker
6. MapStruct

## Instalation prerequistes
1. Java 19
2. Jakarta EE
3. Glassfish V6.0
4. Aerospike CE

## postmanAPi
[Links](https://documenter.getpostman.com/view/25375954/2s935rL3gF)
## Diagrams

### Data flow Diagram, REST requests
![Data flow diagram](digrams/Logic.drawio.png)

### Database digram
![Database digram](digrams/Db.drawio.png)