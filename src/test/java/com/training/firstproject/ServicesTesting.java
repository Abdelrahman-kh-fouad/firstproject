package com.training.firstproject;

import com.aerospike.client.Record;
import com.training.firstproject.DataBase.DBAccess;
import com.training.firstproject.Exceptions.RecordNotFound;
import com.training.firstproject.Exceptions.RedundantRecord;
import com.training.firstproject.Models.DAO.Course;
import com.training.firstproject.Services.CourseServiceImpl;
import com.training.firstproject.Services.ProfessorServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ServicesTesting {
    @BeforeEach
    public void beforeAll() throws RedundantRecord {
        Generating.generate();
    }
    @Test
    void testingCourseGetAssertion() {
        RecordNotFound thrown = Assertions.assertThrows(RecordNotFound.class, () -> {
            CourseServiceImpl.getInstance().getCourse("F12");
        }, "There isn't Course with this key");
        Assertions.assertEquals(thrown.getMessage() , "There isn't Course with this key");
    }
    @Test
    void testingCourseAddAssertion() {
        RedundantRecord thrown = Assertions.assertThrows(RedundantRecord.class, () -> {
            CourseServiceImpl.getInstance().addCourse(CourseServiceImpl.getInstance().getCourse("cs006"));
        }, "There is a Course with this key");
        Assertions.assertEquals(thrown.getMessage() , "There is a Course with this key");
    }
    @Test
    void testingCoursesSet() {
        RecordNotFound thrown = Assertions.assertThrows(RecordNotFound.class, () -> {
            DBAccess.reset();
            CourseServiceImpl.getInstance().getAllCourses(0);
        }, "Courses set is empty");
        Assertions.assertEquals(thrown.getMessage(), "Courses set is empty");
    }
    @Test
    void testingProfessorAdd() {
        RedundantRecord thrown = Assertions.assertThrows(RedundantRecord.class, () -> {
            ProfessorServiceImpl.getInstance().addProfessor(ProfessorServiceImpl.getInstance().getProfesor(1001, 0));
        }, "There is a Course with this key");
        Assertions.assertEquals(thrown.getMessage() , "There is a Course with this key");
    }

}
