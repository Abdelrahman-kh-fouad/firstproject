package com.training.firstproject;

import com.aerospike.mapper.tools.AeroMapper;
import com.training.firstproject.DataBase.DBAccess;
import com.training.firstproject.Exceptions.RecordNotFound;
import com.training.firstproject.Exceptions.RedundantRecord;
import com.training.firstproject.Models.DAO.Course;
import com.training.firstproject.Models.DAO.Department;
import com.training.firstproject.Models.DAO.Professor;
import com.training.firstproject.Models.DAO.Student;
import com.training.firstproject.Services.*;

import java.util.ArrayList;
import java.util.List;

public class Generating {
    /**
     * Constructor for Generating class for making dummy database example.
     * @throws RedundantRecord
     */
    public static void generate() throws RedundantRecord {
        DBAccess.reset();
        // cs dep courses
        Course cs006 = new Course("cs006", "Introduction to algorithms", 4);
        Course cs005 = new Course("cs005", "Copmutational theory", 5);
        // ce dep Courses
        Course ce23 = new Course("ce23", "Computer Architecture", 5);
        Course ce50 = new Course("ce50", "Distributed System", 5);

        CourseServiceImpl.getInstance().addCourse(cs005);
        CourseServiceImpl.getInstance().addCourse(cs006);
        CourseServiceImpl.getInstance().addCourse(ce23);
        CourseServiceImpl.getInstance().addCourse(ce50);
        // Professors
        ArrayList<Course>p1List = new ArrayList<>();
        p1List.add(cs005);
        p1List.add(cs006);
        Professor p1 = new Professor(1001, "AHmed muhammed", p1List);

        ArrayList<Course>p2List = new ArrayList<>();
        p2List.add(ce23);
        p2List.add(ce50);
        Professor p2 = new Professor(1002, "Khaled muhammed", p2List);
        ProfessorServiceImpl.getInstance().addProfessor(p1);
        ProfessorServiceImpl.getInstance().addProfessor(p2);

        // Students
        Student s1 = new Student(2001, "Khaled Fouad", List.of(new Course[]{cs006, ce50}));
        Student s2 = new Student(2002, "Ahmed El-Shazly", List.of(new Course[]{cs006, ce50, ce23}));
        Student s3 = new Student(2003, "Hanaa Ghnam", List.of(new Course[]{cs006, ce50}));
        Student s4 = new Student(2004, "John von Neumann", List.of(new Course[]{cs006, ce50}));
        StudentServiceImpl.getInstance().addStudent(s1);
        StudentServiceImpl.getInstance().addStudent(s2);
        StudentServiceImpl.getInstance().addStudent(s3);
        StudentServiceImpl.getInstance().addStudent(s4);

        // Departments
        Department cs = new Department("cs", "Computer science", List.of(new Professor[]{p1}), List.of(new Student[]{s1 ,s2}));
        Department ce = new Department("ce", "Computer Engineering", List.of(new Professor[]{p2}), List.of(new Student[]{s3 ,s4}));
        DepartmentServiceImpl.getInstance().addDepartment(cs);
        DepartmentServiceImpl.getInstance().addDepartment(ce);
    }

    public static void main(String[] args) throws RedundantRecord, RecordNotFound {
        Generating.generate();
        System.out.println(CourseServiceImpl.getInstance().getCourse("FFff"));
    }
}
