package com.training.firstproject.Controller.SOUP;

import com.training.firstproject.Exceptions.RecordNotFound;
import com.training.firstproject.Exceptions.RedundantRecord;
import com.training.firstproject.Models.DAO.Course;
import com.training.firstproject.Models.DAO.Student;
import com.training.firstproject.Models.DTO.CourseDto;
import com.training.firstproject.Models.DTO.StudentDto;
import com.training.firstproject.Services.CourseService;
import com.training.firstproject.Services.CourseServiceImpl;
import com.training.firstproject.Services.StudentService;
import com.training.firstproject.Services.StudentServiceImpl;
import jakarta.jws.WebMethod;
import jakarta.jws.WebService;
import jakarta.servlet.annotation.WebServlet;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@WebService
public class SoapServices {
    @WebMethod(operationName = "add_student")
    public void addStudent(int id, String name) {
        List<Course> courseList = new ArrayList<>();
        Student student = new Student(id, name, courseList);
        StudentServiceImpl.getInstance().addStudent(student);
    }
    @WebMethod(operationName = "remove_student")
    public void removeStudent(int id) throws RecordNotFound {
        StudentServiceImpl.getInstance().removeStudent(
                StudentServiceImpl.getInstance().getStudent(id, 0));
    }

}
