package com.training.firstproject.Controller.REST;

import com.training.firstproject.Auth.FilterRoles;
import com.training.firstproject.Auth.Role;
import com.training.firstproject.Exceptions.RecordNotFound;
import com.training.firstproject.Models.DAO.Course;
import com.training.firstproject.Models.DAO.Student;
import com.training.firstproject.Models.DTO.CourseDto;
import com.training.firstproject.Models.DTO.StudentDto;
import com.training.firstproject.Models.Mapper.CourseMapper;
import com.training.firstproject.Models.Mapper.StudentMapper;
import com.training.firstproject.Services.ProfessorServiceImpl;
import com.training.firstproject.Services.StudentServiceImpl;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

/**
 * API related to students with path: /student
 */
@Path("/student")
public class StudentController {
    /**
     * @param roleID role of sender(Admin, Student, Professor, Visitor)
     * @return list of all students in the set.
     * @throws RecordNotFound
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllStudent(@Context HttpServletRequest httpServletRequest) {
        List<Student> result = StudentServiceImpl.getInstance()
                .getAllStudent(Integer.parseInt(httpServletRequest.getHeader("role")));
        return Response.status(Response.Status.ACCEPTED)
                .entity(StudentMapper.instance.studentToStudentDto(result)).build();
    }

    @GET
    @Path("/{student_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStudent(@Context HttpServletRequest httpServletRequest, @PathParam("student_id") int studentID) {
        Student result = StudentServiceImpl.getInstance()
                .getStudent(studentID, Integer.parseInt(httpServletRequest.getHeader("role")));
        return Response.status(Response.Status.ACCEPTED)
                .entity(StudentMapper.instance.studentToStudentDto(result)).build();
    }
    @GET
    @Path("/{student_id}/courses")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStudentCourses(@Context HttpServletRequest httpServletRequest, @PathParam("student_id") int studentID) {
        List<Course> result = StudentServiceImpl.getInstance()
                .getMyCourses(studentID, Integer.parseInt(httpServletRequest.getHeader("role")));
        return Response.status(Response.Status.ACCEPTED)
                .entity(CourseMapper.intance.courseToCourseDto(result)).build();
    }

}
