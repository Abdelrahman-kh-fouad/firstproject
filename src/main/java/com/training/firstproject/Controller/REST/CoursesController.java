package com.training.firstproject.Controller.REST;

import com.training.firstproject.Auth.FilterRoles;
import com.training.firstproject.Auth.Role;
import com.training.firstproject.Exceptions.RecordNotFound;
import com.training.firstproject.Models.DAO.Course;
import com.training.firstproject.Models.DAO.Student;
import com.training.firstproject.Models.DTO.CourseDto;
import com.training.firstproject.Models.Mapper.CourseMapper;
import com.training.firstproject.Models.Mapper.CourseMapperImpl;
import com.training.firstproject.Services.CourseService;
import com.training.firstproject.Services.CourseServiceImpl;
import com.training.firstproject.Services.ProfessorServiceImpl;
import com.training.firstproject.Services.StudentServiceImpl;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Request;
import jakarta.ws.rs.core.Response;
import org.mapstruct.factory.Mappers;

import javax.validation.constraints.Positive;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * APIs related to Courses with path /course
 */
@Path("/courses")
public class CoursesController {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response allCourses(@Context HttpServletRequest httpServletRequest) {
        List<Course> result = CourseServiceImpl.getInstance()
                .getAllCourses(Integer.parseInt(httpServletRequest.getHeader("role")));

        return Response.status(Response.Status.ACCEPTED)
                .entity(CourseMapper.intance.courseToCourseDto(result)).build();
    }

    @GET
    @Path("/{course_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCourse(@Context HttpServletRequest httpServletRequest, @PathParam("course_id") String courseID) {
        Course result = CourseServiceImpl.getInstance().getCourse(courseID);
        return Response.status(Response.Status.ACCEPTED)
                .entity(CourseMapper.intance.courseToCourseDto(result)).build();
    }

}
