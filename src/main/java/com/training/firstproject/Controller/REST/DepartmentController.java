package com.training.firstproject.Controller.REST;

import com.training.firstproject.Models.DAO.Department;
import com.training.firstproject.Models.Mapper.DepartmentMapper;
import com.training.firstproject.Services.DepartmentServiceImpl;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.util.List;

@Path("/department")
public class DepartmentController {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllDepartments(@Context HttpServletRequest httpServletRequest) {
        List<Department> result = DepartmentServiceImpl
                .getInstance().getDepartments(Integer.parseInt(httpServletRequest.getHeader("role")));
        return Response.status(Response.Status.ACCEPTED)
                .entity(DepartmentMapper.instance.departmentToDepartmrntDto(result)).build();
    }
}
