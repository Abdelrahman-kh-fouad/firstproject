package com.training.firstproject.Controller.REST;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;

@ApplicationPath("/API")
public class RestApplication extends Application {
}
