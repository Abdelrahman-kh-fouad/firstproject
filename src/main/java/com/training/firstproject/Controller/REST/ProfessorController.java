package com.training.firstproject.Controller.REST;

import com.training.firstproject.Exceptions.RecordNotFound;
import com.training.firstproject.Models.DAO.Course;
import com.training.firstproject.Models.DAO.Professor;
import com.training.firstproject.Models.DTO.CourseDto;
import com.training.firstproject.Models.Mapper.CourseMapper;
import com.training.firstproject.Models.Mapper.ProfessorMapper;
import com.training.firstproject.Services.ProfessorServiceImpl;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.util.List;

/**
 * Professor Related API with Path: /professor
 */
@Path("/professor")
public class ProfessorController {
    /**
     * @param roleID role of sender(Admin, Student, Professor, Visitor)
     * @return list of all professors in the set
     * @throws RecordNotFound
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllProfessors(@Context HttpServletRequest httpServletRequest) {
        List<Professor> result = ProfessorServiceImpl.getInstance()
                .getAllProfessors(Integer.parseInt(httpServletRequest.getHeader("role")));
        return Response.status(Response.Status.ACCEPTED)
                .entity(ProfessorMapper.instance.professorToProfessorDto(result)).build();
    }

    @GET
    @Path("/{professor_id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProfessor(@Context HttpServletRequest httpServletRequest, @PathParam("professor_id") int professorID) {
        Professor result = ProfessorServiceImpl.getInstance().getProfesor(
                professorID, Integer.parseInt(httpServletRequest.getHeader("role")));
        return Response.status(Response.Status.ACCEPTED)
                .entity(ProfessorMapper.instance.professorToProfessorDto(result)).build();
    }

    @GET
    @Path("/{professor_id}/courses")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProfessorCourses(@Context HttpServletRequest httpServletRequest, @PathParam("professor_id") int professorID) {
        List<Course> result = ProfessorServiceImpl.getInstance().getMyCourses(
                professorID
                ,Integer.parseInt(httpServletRequest.getHeader("role")));
        return Response.status(Response.Status.ACCEPTED)
                .entity(CourseMapper.intance.courseToCourseDto(result)).build();
    }

    @POST
    @Path("/{professor_id}/courses")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addProfessorCourses(@Context HttpServletRequest httpServletRequest, @PathParam("professor_id") int professorID,CourseDto courseDto) {
        ProfessorServiceImpl.getInstance().addProfessorCourses(
            professorID, CourseMapper.intance.courseDtoToCourse(courseDto)
            ,Integer.parseInt(httpServletRequest.getHeader("role")));
    return Response.status(Response.Status.ACCEPTED).build();
    }
    @DELETE
    @Path("/{professor_id}/courses")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteProfessorCourses(@Context HttpServletRequest httpServletRequest, @PathParam("professor_id") int professorID,CourseDto courseDto) {
        ProfessorServiceImpl.getInstance().deleteProfessorCourses(
            professorID
            , CourseMapper.intance.courseDtoToCourse(courseDto)
            ,Integer.parseInt(httpServletRequest.getHeader("role")));
        return Response.status(Response.Status.ACCEPTED).build();
    }

}
