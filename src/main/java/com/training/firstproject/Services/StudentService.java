package com.training.firstproject.Services;

import com.training.firstproject.Exceptions.AccessDenied;
import com.training.firstproject.Exceptions.RecordNotFound;
import com.training.firstproject.Exceptions.RedundantRecord;
import com.training.firstproject.Models.DAO.Course;
import com.training.firstproject.Models.DAO.Professor;
import com.training.firstproject.Models.DAO.Student;

import java.util.ArrayList;
import java.util.List;

public interface StudentService {
    public List<Student> getAllStudent(int roleID);
    public Student getStudent(int studentID, int roleID);
    public void addStudent(Student student) throws RedundantRecord;
    public void editStudent(Student student) throws RecordNotFound;
    public void removeStudent(Student student) throws RecordNotFound;
    public List<Course> getMyCourses(int studentID, int roleID);
    public void addCourse(int studentID, Course course);
}
