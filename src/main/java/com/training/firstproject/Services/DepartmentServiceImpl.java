package com.training.firstproject.Services;

import com.training.firstproject.Auth.FilterRoles;
import com.training.firstproject.Auth.Role;
import com.training.firstproject.DataBase.DBAccess;
import com.training.firstproject.Exceptions.AccessDenied;
import com.training.firstproject.Exceptions.RecordNotFound;
import com.training.firstproject.Exceptions.RedundantRecord;
import com.training.firstproject.Exceptions.UnlogicalOperation;
import com.training.firstproject.Models.DAO.Department;
import com.training.firstproject.Models.DAO.Professor;
import com.training.firstproject.Models.DAO.Student;

import javax.swing.plaf.SeparatorUI;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Department service implementation.
 */
public class DepartmentServiceImpl implements DepartmentService{
    private static DepartmentService instance;


    @Override
    public List<Department> getDepartments(int roleID) {
        DBAccess<Department> departmentRepository = new DBAccess<>(Department.class);
        Role role = FilterRoles.getRole(roleID);
        if(role != Role.Admin) {
            throw new AccessDenied();
        } else {
            return departmentRepository.getSet();
        }
    }

    /**
     * @param department Deaprtment you want to add.
     * @throws RedundantRecord
     */
    @Override
    public void addDepartment(Department department) {
        DBAccess<Department> departmentRepository = new DBAccess<>(Department.class);
        Department found = departmentRepository.getObject(department.getCodeName());
        if (found == null) {
            departmentRepository.saveObject(department);
        } else {
            throw new RedundantRecord("There is Departmaent with the same key");
        }
    }

    /**
     * @param department Department that will be modified.
     * @param professor Professor that will be added to department
     * @throws RedundantRecord
     */
    @Override
    public void addProfessor(Department department, Professor professor) throws RedundantRecord {
        DBAccess<Professor> professorRepository = new DBAccess<>(Professor.class);
        DBAccess<Department> departmentRepository = new DBAccess<>(Department.class);
        try {
            ProfessorServiceImpl.getInstance().getProfesor(professor.getId(), 0);
            throw new RedundantRecord("There is a Professor with this key");
        } catch (RecordNotFound e) {
            professorRepository.saveObject(professor);
            department.getProfessorList().add(professor);
            departmentRepository.saveObject(department);
        }
    }

    /**
     * @param department Department that will be modified.
     * @param student Student that will be added to Department.
     * @throws RedundantRecord
     */
    @Override
    public void addStudent(Department department, Student student) throws RedundantRecord {
        DBAccess<Student> studentRepository = new DBAccess<>(Student.class);
        DBAccess<Department> departmentRepository = new DBAccess<>(Department.class);
        try {
            StudentServiceImpl.getInstance().getStudent(student.getId(), 0);
            throw new RedundantRecord("There is a Student with this key");
        } catch (RecordNotFound e) {
            studentRepository.saveObject(student);
            department.getStudentList().add(student);
            departmentRepository.saveObject(department);
        }
    }
    public static DepartmentService getInstance() {
        if (instance == null) {
            instance = new DepartmentServiceImpl();
        }
        return instance;
    }
}
