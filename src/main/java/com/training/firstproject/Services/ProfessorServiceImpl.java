package com.training.firstproject.Services;

import com.training.firstproject.Auth.FilterRoles;
import com.training.firstproject.Auth.Role;
import com.training.firstproject.DataBase.DBAccess;
import com.training.firstproject.Exceptions.AccessDenied;
import com.training.firstproject.Exceptions.RecordNotFound;
import com.training.firstproject.Exceptions.RedundantRecord;
import com.training.firstproject.Models.DAO.Course;
import com.training.firstproject.Models.DAO.Professor;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Professor service implementation.
 */
public class ProfessorServiceImpl implements ProfessorService{
    private static ProfessorService instance;


    /**
     * @return List<Professors> list of professors of the set.
     * @throws RecordNotFound
     */
    @Override
    public List<Professor> getAllProfessors(int roleID) {
        DBAccess<Professor> professorReposotiry = new DBAccess<>(Professor.class);
        List<Professor> res = null;
        Role role = FilterRoles.getRole(roleID);
        if (role != Role.Admin) {
            throw new AccessDenied();
        } else {
            res = professorReposotiry.getSet();
        }
        return res;
    }

    /**
     * @param id Professor's key you want to get.
     * @return professor object.
     * @throws RecordNotFound
     */
    @Override
    public Professor getProfesor(int professorID, int roleID) {
        DBAccess<Professor> professorReposotiry = new DBAccess<>(Professor.class);
        Professor res = null;
        Role role = FilterRoles.getRole(roleID);
        if(role == Role.Visitor) {
            throw new AccessDenied();
        } else {
            res = professorReposotiry.getObject(professorID);
            if (res == null) {
                throw new RecordNotFound("There isn't Professor with this key");
            }
        }
        return res;
    }

    @Override
    public void addProfessorCourses(int professorID, Course course, int roleID) {
        Role role = FilterRoles.getRole(roleID);
        if(role != Role.Admin) {
            throw new AccessDenied();
        } else {
            Professor professor = getProfesor(professorID, roleID);
            CourseServiceImpl.getInstance().addCourse(course);
            professor.getCourseList().add(course);
            ProfessorServiceImpl.getInstance().editProfessor(professor);
        }
    }

    @Override
    public void deleteProfessorCourses(int professorID, Course course, int roleID) {
        Role role = FilterRoles.getRole(roleID);
        if(role != Role.Admin) {
            throw new AccessDenied();
        } else {
            Professor professor = getProfesor(professorID, roleID);
            CourseServiceImpl.getInstance().deleteCourse(course);
            professor.setCourseList(professor.getCourseList().stream()
                    .filter((x) -> {return  x.getCodeName()!= course.getCodeName();}).collect(Collectors.toList()));
            ProfessorServiceImpl.getInstance().editProfessor(professor);
        }
    }

    /**
     * @param professor Professor will be added.
     * @throws RedundantRecord
     */
    @Override
    public void addProfessor(Professor professor) throws RedundantRecord {
        DBAccess<Professor> professorReposotiry = new DBAccess<>(Professor.class);
        Professor found = professorReposotiry.getObject(professor.getId());
        if(found == null) {
            professorReposotiry.saveObject(professor);
        } else {
            throw new RedundantRecord("There is a Professor with this key");
        }
    }

    /**
     * @param professor Professor that will be edited.
     * @throws RecordNotFound
     */
    @Override
    public void editProfessor(Professor professor) throws RecordNotFound {
        DBAccess<Professor> professorReposotiry = new DBAccess<>(Professor.class);
        Professor found = professorReposotiry.getObject(professor);
        if(found != null) {
            professorReposotiry.saveObject(professor);
        } else {
            throw new RecordNotFound("There isn't Professor with this key");
        }
    }

    /**
     * @param professor that will be deleted.
     * @throws RecordNotFound
     */
    @Override
    public void removeProfessor(Professor professor) throws RecordNotFound {
        DBAccess<Professor> professorReposotiry = new DBAccess<>(Professor.class);
        Professor found = professorReposotiry.getObject(professor.getId());
        if(found != null) {
            professorReposotiry.delete(professor);
        } else {
            throw new RecordNotFound("There isn't Professor with this key");
        }
    }

    /**
     * @param professor Professor with courses.
     * @return list of courses.
     */
    @Override
    public List<Course> getMyCourses(int professorID, int roleID) {
        Role role = FilterRoles.getRole(roleID);
        Professor professor = getProfesor(professorID, 0);
        return professor.getCourseList();
    }
    public static ProfessorService getInstance() {
        if (instance == null) {
            instance = new ProfessorServiceImpl();
        }
        return instance;
    }
}
