package com.training.firstproject.Services;

import com.training.firstproject.Auth.Role;
import com.training.firstproject.Exceptions.AccessDenied;
import com.training.firstproject.Exceptions.RecordNotFound;
import com.training.firstproject.Exceptions.RedundantRecord;
import com.training.firstproject.Models.DAO.Course;

import java.util.List;

public interface CourseService {
    public List<Course> getAllCourses(int roleID);
    public Course getCourse(String key);
    public void addCourse(Course course);
    public void deleteCourse(Course course);
}
