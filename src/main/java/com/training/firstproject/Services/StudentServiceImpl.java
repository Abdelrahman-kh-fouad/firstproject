package com.training.firstproject.Services;

import com.training.firstproject.Auth.FilterRoles;
import com.training.firstproject.Auth.Role;
import com.training.firstproject.DataBase.DBAccess;
import com.training.firstproject.Exceptions.AccessDenied;
import com.training.firstproject.Exceptions.RecordNotFound;
import com.training.firstproject.Exceptions.RedundantRecord;
import com.training.firstproject.Exceptions.UnlogicalOperation;
import com.training.firstproject.Models.DAO.Course;
import com.training.firstproject.Models.DAO.Professor;
import com.training.firstproject.Models.DAO.Student;

import java.util.List;

/**
 * Student service implementation.
 */
public class StudentServiceImpl implements StudentService {
    private static StudentServiceImpl instance;

    /**
     * @return List<Student> all students on the set.
     * @throws RecordNotFound
     */
    @Override
    public List<Student> getAllStudent(int roleID) {
        DBAccess<Student > studentReposotiry = new DBAccess<>(Student.class);
        List<Student> res = null;
        Role role = FilterRoles.getRole(roleID);
        if (role != Role.Admin) {
            throw new AccessDenied();
        } else {
            res = studentReposotiry.getSet();
        }
        return res;
    }

    /**
     * @param studentID idStudent is the key to the record.
     * @return student object.
     * @throws RecordNotFound
     */
    @Override
    public Student getStudent(int studentID, int roleID){
        DBAccess<Student> studentReposotery = new DBAccess<>(Student.class);
        Role role = FilterRoles.getRole(roleID);
        Student result = null;
        if(role == Role.Professor || role == Role.Admin) {
            result = studentReposotery.getObject(studentID);
            if(result == null) {
                throw new RecordNotFound("There isn't Student with this key");
            }
        }
        return result;
    }

    /**
     * @param student student will be added.
     * @throws RedundantRecord
     */
    @Override
    public void addStudent(Student student) {
        DBAccess<Student> studentReposotery = new DBAccess<>(Student.class);
        Student found = studentReposotery.getObject(student.id);
        if(found == null) {
            throw new RedundantRecord("There is a Student with this key");
        }
        studentReposotery.saveObject(student);
    }

    /**
     * @param student Student will be edited.
     * @throws RecordNotFound
     */
    @Override
    public void editStudent(Student student) throws RecordNotFound {
        DBAccess<Student> studentReposotery = new DBAccess<>(Student.class);
        Student found = studentReposotery.getObject(student);
        if (found == null) {
            throw new RecordNotFound("There isn't Student with this key");
        }
        studentReposotery.saveObject(student);
    }

    /**
     * @param student Student will be Deleted.
     * @throws RecordNotFound
     */
    @Override
    public void removeStudent(Student student) throws RecordNotFound {
        DBAccess<Student> studentReposotery = new DBAccess<>(Student.class);
        Student found = studentReposotery.getObject(student);
        if (found == null) {
            throw new RecordNotFound("There isn't Student with this key");
        }
        studentReposotery.delete(student);
    }

    /**
     * @param student Student you want to get his courses.
     * @return List<Course> list of courses.
     * @throws RecordNotFound
     */
    @Override
    public List<Course> getMyCourses(int studentID, int roleID) {
        return getStudent(studentID, 0).getCourseList();
    }

    @Override
    public void addCourse(int studentID, Course course) {
        DBAccess<Course> courseRepo = new DBAccess<>(Course.class);
        DBAccess<Student> studentRepo = new DBAccess<>(Student.class);
        Student student = StudentServiceImpl.getInstance().getStudent(studentID, 0);
        Course found = courseRepo.getObject(course);
        if(found == null) {
            throw new UnlogicalOperation("No one teachs this course");
        } else {
            if(student.getCourseList().stream().filter(x -> {return x.getCodeName()== course.getCodeName();}).count() ==0) {
                courseRepo.saveObject(course);
                student.getCourseList().add(course);
                studentRepo.saveObject(student);
            }
        }
    }

    public static StudentServiceImpl getInstance() {
        if (instance == null) {
            instance = new StudentServiceImpl();
        }
        return instance;
    }
}
