package com.training.firstproject.Services;

import com.training.firstproject.Exceptions.RecordNotFound;
import com.training.firstproject.Exceptions.RedundantRecord;
import com.training.firstproject.Models.DAO.Course;
import com.training.firstproject.Models.DAO.Professor;
import com.training.firstproject.Models.DAO.Student;

import java.util.List;

public interface ProfessorService {

    public List<Professor> getAllProfessors(int roleID);
    public Professor getProfesor(int professorID, int roleID) ;
    public void addProfessorCourses(int professorID, Course course, int roleID);
    public void deleteProfessorCourses(int professorID, Course course, int roleID);

    public void addProfessor(Professor professor);
    public void editProfessor(Professor professor);
    public void removeProfessor(Professor professor);
    public List<Course> getMyCourses(int professorID, int roleID);
}
