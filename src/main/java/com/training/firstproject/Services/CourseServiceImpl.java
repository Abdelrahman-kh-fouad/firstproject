package com.training.firstproject.Services;

import com.training.firstproject.Auth.FilterRoles;
import com.training.firstproject.Auth.Role;
import com.training.firstproject.DataBase.DBAccess;
import com.training.firstproject.DataBase.DataBase;
import com.training.firstproject.Exceptions.AccessDenied;
import com.training.firstproject.Exceptions.RecordNotFound;
import com.training.firstproject.Exceptions.RedundantRecord;
import com.training.firstproject.Models.DAO.Course;
import com.training.firstproject.Models.DTO.CourseDto;
import com.training.firstproject.Models.Mapper.CourseMapper;
import jakarta.ws.rs.core.Response;

import java.util.ArrayList;
import java.util.List;

/**
 * Courses service implementation.
 */
public class CourseServiceImpl implements CourseService{
    private static CourseService instance;

    /**
     * @return List<Course> all course set.
     * @throws RecordNotFound
     */
    @Override
    public List<Course> getAllCourses(int roleID) {
        DBAccess<Course> coursesRepository = new DBAccess<>(Course.class);
        List<Course> res = null;
        Role role = FilterRoles.getRole(roleID);
        if (role != Role.Admin) {
            throw new AccessDenied();
        } else {
            res = coursesRepository.getSet();
        }
        return res;
    }

    /**
     * @param key: course key like: "Cs6.006"
     * @return Course object
     * @throws RecordNotFound
     */
    @Override
    public Course getCourse(String key) {
        DBAccess<Course> coursesRepository = new DBAccess<>(Course.class);
        Course  res =  coursesRepository.getObject(key);
        if (res == null) {
            throw new RecordNotFound("There isn't Course with this key");
        }
        return res;
    }

    /**
     * @param course Course that you want to add to set.
     * @throws RedundantRecord
     */
    @Override
    public void addCourse(Course course) {
        DBAccess<Course> coursesRepository = new DBAccess<>(Course.class);
        Course found = coursesRepository.getObject(course.getCodeName());
        if (found == null) {
            coursesRepository.saveObject(course);
        } else {
            throw new RedundantRecord("There is a Course with this key");
        }
    }

    /**
     * @param course Course you want to delete.
     * @throws RecordNotFound
     */
    @Override
    public void deleteCourse(Course course) throws RecordNotFound {
        DBAccess<Course> coursesRepository = new DBAccess<>(Course.class);
        Course found = coursesRepository.getObject(course.getCodeName());
        if (found != null) {
            coursesRepository.delete(course);
        } else {
            throw new RecordNotFound("There isn't course with this key");
        }
    }
    public static CourseService getInstance() {
        if (instance == null) {
            instance = new CourseServiceImpl();
        }
        return instance;
    }
}
