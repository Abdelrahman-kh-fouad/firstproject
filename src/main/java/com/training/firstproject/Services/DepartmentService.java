package com.training.firstproject.Services;

import com.training.firstproject.Exceptions.RedundantRecord;
import com.training.firstproject.Exceptions.UnlogicalOperation;
import com.training.firstproject.Models.DAO.Department;
import com.training.firstproject.Models.DAO.Professor;
import com.training.firstproject.Models.DAO.Student;

import java.util.List;

public interface DepartmentService {
    public List<Department> getDepartments(int roleID);
    public void addDepartment(Department department) throws RedundantRecord;
    public void addProfessor(Department department, Professor professor) throws RedundantRecord;
    public void addStudent(Department department, Student student) throws RedundantRecord;
}
