package com.training.firstproject.Exceptions;

import com.training.firstproject.Models.ErrorMessage;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

@Provider
public class AccessDeniedMapper implements ExceptionMapper<AccessDenied> {
    @Override
    public Response toResponse(AccessDenied exception) {
        ErrorMessage errorMessage = new ErrorMessage(exception.getMessage(), 403 );
        return Response.status(Response.Status.FORBIDDEN).entity(errorMessage).build();
    }
}
