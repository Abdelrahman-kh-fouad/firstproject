package com.training.firstproject.Exceptions;

public class RecordNotFound extends RuntimeException {
    public RecordNotFound(String s) {
        super(s);
    }
}
