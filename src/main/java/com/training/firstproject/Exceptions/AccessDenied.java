package com.training.firstproject.Exceptions;

public class AccessDenied extends RuntimeException{
    public AccessDenied() {
        super("Access Denied, Unauthorized");
    }
}
