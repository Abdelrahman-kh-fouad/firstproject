package com.training.firstproject.Exceptions;

public class RedundantRecord extends RuntimeException {
    public RedundantRecord(String s) {
        super(s);
    }
}
