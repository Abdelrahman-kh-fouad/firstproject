package com.training.firstproject.Exceptions;

import com.training.firstproject.Models.ErrorMessage;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

@Provider
public class RecordNotFoundMapper implements ExceptionMapper<RecordNotFound> {
    @Override
    public Response toResponse(RecordNotFound exception) {
        ErrorMessage errorMessage = new ErrorMessage(exception.getMessage(), 404);
        return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
    }
}
