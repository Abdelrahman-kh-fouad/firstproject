package com.training.firstproject.Auth;

public class FilterRoles {
    public static Role getRole(int roleInt) {
        Role res = Role.Visitor;
        switch (roleInt) {
            case 0:
                res = Role.Admin;
                break;
            case 1:
                res = Role.Professor;
                break;
            case 2:
                res = Role.Student;
                break;
            default:
                break;
        }
        return res;
    }
}
