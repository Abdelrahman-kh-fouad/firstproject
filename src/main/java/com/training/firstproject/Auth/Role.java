package com.training.firstproject.Auth;

public enum Role {
    Admin, Professor, Student, Visitor
}
