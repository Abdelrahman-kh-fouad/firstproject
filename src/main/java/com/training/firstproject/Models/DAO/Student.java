package com.training.firstproject.Models.DAO;
import com.training.firstproject.Constants.DB;

import com.aerospike.mapper.annotations.AerospikeConstructor;
import com.aerospike.mapper.annotations.AerospikeKey;
import com.aerospike.mapper.annotations.AerospikeRecord;
import com.aerospike.mapper.annotations.AerospikeReference;

import java.util.List;

@AerospikeRecord(namespace = DB.NAMESPACE, set = DB.STUDENTS_SET)
public class Student {

    @AerospikeKey
    public int id;
    public String name;

    @AerospikeReference
    public List<Course> courseList;

    public Student(){}

    /**
     * Basic constructor for Student class.
     * @param id student id.
     * @param name student full name.
     * @param courseList courses he enrolled in.
     */
    public Student(int id, String name, List<Course> courseList) {
        this.id = id;
        this.name = name;
        this.courseList = courseList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Course> getCourseList() {
        return courseList;
    }

    public void setCourseList(List<Course> courseList) {
        this.courseList = courseList;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", courseList=" + courseList +
                '}';
    }
}
