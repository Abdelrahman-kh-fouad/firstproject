package com.training.firstproject.Models.DAO;
import com.training.firstproject.Constants.DB;
import com.aerospike.mapper.annotations.AerospikeConstructor;
import com.aerospike.mapper.annotations.AerospikeKey;
import com.aerospike.mapper.annotations.AerospikeRecord;
import com.aerospike.mapper.annotations.AerospikeReference;

import java.util.List;
@AerospikeRecord(namespace = DB.NAMESPACE, set = DB.DEPARTMENT_SET)
public class Department {

    @AerospikeKey
    private String codeName;
    private String name;
    @AerospikeReference
    private List<Professor>professorList;
    @AerospikeReference
    private List<Student>studentList;
    public Department(){}

    /**
     * Basic constructor for Department.
     * @param codeName Department's short name like: CSE.
     * @param name Department name.
     * @param professorList Professors.
     * @param studentList Students.
     */
    public Department(String codeName, String name, List<Professor> professorList, List<Student> studentList) {
        this.codeName = codeName;
        this.name = name;
        this.professorList = professorList;
        this.studentList = studentList;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Professor> getProfessorList() {
        return professorList;
    }

    public void setProfessorList(List<Professor> professorList) {
        this.professorList = professorList;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }
}
