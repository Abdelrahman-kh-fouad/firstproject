package com.training.firstproject.Models.DAO;
import com.training.firstproject.Constants.DB;

import com.aerospike.mapper.annotations.AerospikeConstructor;
import com.aerospike.mapper.annotations.AerospikeKey;
import com.aerospike.mapper.annotations.AerospikeRecord;
import com.aerospike.mapper.annotations.AerospikeReference;

import java.util.List;

@AerospikeRecord(namespace = DB.NAMESPACE, set = DB.PROFESSOR_SET)
public class Professor {

    @AerospikeKey
    private int id;
    private String name;
    @AerospikeReference
    private List<Course> courseList;

    public Professor(){}

    /**
     * Basic constructor for professor class.
     * @param id Professor id.
     * @param name professor full name.
     * @param courseList courses he teachs.
     */
    public Professor(int id, String name, List<Course> courseList) {
        this.id = id;
        this.name = name;
        this.courseList = courseList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Course> getCourseList() {
        return courseList;
    }

    public void setCourseList(List<Course> courseList) {
        this.courseList = courseList;
    }
}
