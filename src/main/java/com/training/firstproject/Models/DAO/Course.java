package com.training.firstproject.Models.DAO;
import com.training.firstproject.Constants.DB;
import com.aerospike.mapper.annotations.AerospikeConstructor;
import com.aerospike.mapper.annotations.AerospikeKey;
import com.aerospike.mapper.annotations.AerospikeRecord;
import com.aerospike.mapper.annotations.AerospikeReference;

import java.util.List;

@AerospikeRecord(namespace= DB.NAMESPACE, set=DB.COURSE_SET)
public class Course {
    @AerospikeKey
    private String codeName;
    private String courseName;
    private int noGroups;

    public Course() {}

    /**
     * <p>Basic constructor for Course</p>
     * @param codeName course short name like: MTH01, MIT6.006.
     * @param courseName Course name.
     * @param noGroups Number of groups for the course.
     */
    public Course(String codeName, String courseName, int noGroups) {
        this.codeName = codeName;
        this.courseName = courseName;
        this.noGroups = noGroups;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getNoGroups() {
        return noGroups;
    }

    public void setNoGroups(int noGroups) {
        this.noGroups = noGroups;
    }

    @Override
    public String toString() {
        return "Course{" +
                "codeName='" + codeName + '\'' +
                ", courseName='" + courseName + '\'' +
                ", noGroups=" + noGroups +
                '}';
    }
}
