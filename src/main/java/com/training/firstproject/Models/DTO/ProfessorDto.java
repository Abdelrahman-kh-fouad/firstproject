package com.training.firstproject.Models.DTO;

import com.training.firstproject.Models.DAO.Department;
import jakarta.xml.bind.annotation.XmlRootElement;

import java.util.List;
@XmlRootElement

public class ProfessorDto {
    private int id;
    private String name;
    private List<CourseDto> courseList;

    public ProfessorDto() {}
    public ProfessorDto(int id, String name, List<CourseDto> courseList) {
        this.id = id;
        this.name = name;
        this.courseList = courseList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CourseDto> getCourseList() {
        return courseList;
    }

    public void setCourseList(List<CourseDto> courseList) {
        this.courseList = courseList;
    }
}
