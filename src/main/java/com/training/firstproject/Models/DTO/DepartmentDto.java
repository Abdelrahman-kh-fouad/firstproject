package com.training.firstproject.Models.DTO;

import com.training.firstproject.Models.DAO.Department;
import jakarta.xml.bind.annotation.XmlRootElement;

import java.util.List;
@XmlRootElement

public class DepartmentDto {
    private String codeName;
    private String name;

    private List<StudentDto> studentList;
    private List<ProfessorDto> professorList;

    public DepartmentDto(){}

    /**
     * @param codeName
     * @param name
     * @param studentList
     * @param professorList
     */
    public DepartmentDto(String codeName, String name, List<StudentDto> studentList, List<ProfessorDto> professorList) {
        this.codeName = codeName;
        this.name = name;
        this.studentList = studentList;
        this.professorList = professorList;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<StudentDto> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<StudentDto> studentList) {
        this.studentList = studentList;
    }

    public List<ProfessorDto> getProfessorList() {
        return professorList;
    }

    public void setProfessorList(List<ProfessorDto> professorList) {
        this.professorList = professorList;
    }
}
