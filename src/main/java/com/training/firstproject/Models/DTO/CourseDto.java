package com.training.firstproject.Models.DTO;

import com.fasterxml.jackson.annotation.JsonRootName;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CourseDto {
    private String codeName;
    private String courseName;
    private int noGroups;

    public CourseDto(){}

    /**
     *  Constructor for CourseDTO
     * @param codeName Course short name.
     * @param courseName course name.
     * @param noGroups course groups number.
     */
    public CourseDto(String codeName, String courseName, int noGroups) {
        this.codeName = codeName;
        this.courseName = courseName;
        this.noGroups = noGroups;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getNoGroups() {
        return noGroups;
    }

    public void setNoGroups(int noGroups) {
        this.noGroups = noGroups;
    }
}
