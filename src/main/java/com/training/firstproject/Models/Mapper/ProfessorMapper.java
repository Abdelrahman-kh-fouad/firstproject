package com.training.firstproject.Models.Mapper;

import com.training.firstproject.Models.DAO.Professor;
import com.training.firstproject.Models.DTO.ProfessorDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ProfessorMapper {
    ProfessorMapper instance = Mappers.getMapper(ProfessorMapper.class);
    ProfessorDto professorToProfessorDto(Professor professor);
    Professor professorDtoToProfessor(ProfessorDto professorDto);
    List<ProfessorDto> professorToProfessorDto(List<Professor> professorDtosList);
}
