package com.training.firstproject.Models.Mapper;

import com.training.firstproject.Models.DAO.Course;
import com.training.firstproject.Models.DTO.CourseDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface CourseMapper {
    CourseMapper intance = Mappers.getMapper(CourseMapper.class);
    Course courseDtoToCourse(CourseDto courseDto);
    CourseDto courseToCourseDto(Course course);
    List<CourseDto> courseToCourseDto(List<Course> courseList);
}
