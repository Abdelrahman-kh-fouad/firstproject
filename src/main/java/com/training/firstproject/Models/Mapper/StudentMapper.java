package com.training.firstproject.Models.Mapper;

import com.training.firstproject.Exceptions.RecordNotFound;
import com.training.firstproject.Models.DAO.Course;
import com.training.firstproject.Models.DAO.Student;
import com.training.firstproject.Models.DTO.CourseDto;
import com.training.firstproject.Models.DTO.StudentDto;
import com.training.firstproject.Services.StudentServiceImpl;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Mapper
public interface StudentMapper {
    StudentMapper instance = Mappers.getMapper(StudentMapper.class);
    default Student studentDtoToStudent(StudentDto studentDto) throws RecordNotFound {
        return StudentServiceImpl.getInstance().getStudent(studentDto.getId(), 0);
    }
    StudentDto studentToStudentDto(Student student);
    List<StudentDto> studentToStudentDto(List<Student> students);

}
