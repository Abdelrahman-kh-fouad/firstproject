package com.training.firstproject.Models.Mapper;

import com.training.firstproject.Models.DAO.Department;
import com.training.firstproject.Models.DAO.Professor;
import com.training.firstproject.Models.DTO.DepartmentDto;
import com.training.firstproject.Models.DTO.ProfessorDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface DepartmentMapper {
    DepartmentMapper instance = Mappers.getMapper(DepartmentMapper.class);
    List<DepartmentDto> departmentToDepartmrntDto(List<Department> department);

}
