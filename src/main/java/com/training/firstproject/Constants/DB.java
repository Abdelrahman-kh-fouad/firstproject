package com.training.firstproject.Constants;

public class DB {
    public static final String NAMESPACE = "test";
    public static final String COURSE_SET = "courses";
    public static final String COURSE_KEY = "codeName";
    public static final String DEPARTMENT_SET = "departments";
    public static final String DEPARTMENT_KEY = "codeName";
    public static final String PROFESSOR_SET = "professors";
    public static final String PROFESSOR_KEY = "id";
    public static final String STUDENTS_SET = "students";
    public static final String STUDENTS_KEY = "id";
    public static final String IP = "127.0.0.1";
    public static final int PORT = 3000;
}
