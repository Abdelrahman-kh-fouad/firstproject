package com.training.firstproject.DataBase;

import com.aerospike.client.Record;
import com.aerospike.client.query.RecordSet;
import com.aerospike.client.query.Statement;
import com.training.firstproject.Constants.DB;
import com.training.firstproject.Exceptions.RecordNotFound;
import com.training.firstproject.Models.DAO.Course;
import com.training.firstproject.Models.DAO.Department;
import com.training.firstproject.Models.DAO.Professor;
import com.training.firstproject.Models.DAO.Student;
import com.training.firstproject.Models.DTO.CourseDto;
import com.training.firstproject.Models.Mapper.CourseMapper;
import com.training.firstproject.Services.CourseServiceImpl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DBAccess<T> {
    private Class<T> type;
    private static DataBase db = DataBase.getInstance();
    private static final Map<Class<?>, String> classToSet = new HashMap<>();
    private static final Map<Class<?>, String> primaryketOfClass = new HashMap<>();
    static {
        classToSet.put(Student.class, DB.STUDENTS_SET);
        classToSet.put(Department.class, DB.DEPARTMENT_SET);
        classToSet.put(Professor.class, DB.PROFESSOR_SET);
        classToSet.put(Course.class, DB.COURSE_SET);

        primaryketOfClass.put(Student.class, DB.STUDENTS_KEY);
        primaryketOfClass.put(Department.class, DB.DEPARTMENT_KEY);
        primaryketOfClass.put(Professor.class, DB.PROFESSOR_KEY);
        primaryketOfClass.put(Course.class, DB.COURSE_KEY);
    }
    public DBAccess(Class<T> t) {
        this.type = t;
    }
    public List getSet() {
        ArrayList<T> setRecords = new ArrayList<>();
        Statement stmt = new Statement();
        stmt.setNamespace(DB.NAMESPACE);
        stmt.setSetName(classToSet.get(type));
        try (RecordSet recordSet = db.dbClient.query(null, stmt)) {
            while (recordSet.next()) {
                Record aerospikeRecord = recordSet.getRecord();
                Object key = aerospikeRecord.bins.get(primaryketOfClass.get(type));
                setRecords.add(getObject(key));
            }
        }
        return setRecords;
    }
    public T getObject(Object key) {
        return db.dbMapper.read(type, key);
    }
    public void saveObject(T toBeAdded) {
        db.dbMapper.save(toBeAdded);
    }
    public void delete(T toBeDeleted) {
        db.dbMapper.delete(toBeDeleted);
    }
    static public void reset() {
        db.truncate();
    }

}
