package com.training.firstproject.DataBase;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.policy.WritePolicy;
import com.aerospike.mapper.tools.AeroMapper;
import com.training.firstproject.Constants.DB;
import com.training.firstproject.Models.DAO.Course;

public class DataBase {
    public AerospikeClient dbClient;
    public AeroMapper dbMapper;
    private static DataBase instance;

    private DataBase() {
        dbClient = new AerospikeClient(DB.IP, DB.PORT);
        dbMapper = new AeroMapper.Builder(dbClient).build();
    }
    public void truncate() {
        try {
            dbClient.truncate(null, DB.NAMESPACE, null, null);
        }
        catch (AerospikeException e) {
            // ignore
        }
    }
    public static DataBase getInstance() {
        if (instance == null) {
            instance = new DataBase();
        }
        return instance;
    }


//    public static void main(String[] args) {
//        DataBase db = DataBase.getInstance();
//        AeroMapper mapper = new AeroMapper.Builder(db.dbClient).build();
//        Course course = mapper.read(Course.class, "phy2");
//        // mapper.save(course);
//        System.out.println("sd");
//    }
}